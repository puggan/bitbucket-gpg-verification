<?php

	require_once(__DIR__.'/vendor/autoload.php');
	use Kreait\Firebase;

	chdir(__DIR__);
	exec("grep url repos/*.git/config", $remotes_rows);
	$remotes = [];
	foreach($remotes_rows as $row)
	{
		if(preg_match("#^repos/(?<path>.+)/config:\\s+url\\s=\\s(.+)@(.+):(?<remote>.+?)(\\.git)?\\s*\$#", $row, $hits))
		{
			$remotes[$hits['path']] = $hits['remote'];
		}
	}

	if($argc > 2 OR $argc == 2 AND $argv[1] != '-f')
	{
		$remotes = array_intersect_key($remotes, array_combine($argv, $argv));

		if(!$remotes)
		{
			trigger_error("No matching repos");
			die(2);
		}
	}

	if(!$remotes)
	{
		trigger_error("No repos");
		die(1);
	}

	$firebase = (new Firebase\Factory())->withCredentials(__DIR__ . '/firebase.json')->create();
	if(!$firebase) die('DB failed' . PHP_EOL);

	$database = $firebase->getDatabase();
	if(!$database) die('DB failed' . PHP_EOL);

	foreach($remotes as $path => $remote)
	{
		echo PHP_EOL . $remote . ':' . PHP_EOL;
		chdir(__DIR__ . '/repos/' . $path);
		$before = shell_exec("git show-ref");
		exec("git remote", $repo_remotes);
		foreach($repo_remotes as $repo_remote)
		{
			passthru("git fetch " . escapeshellarg($repo_remote) . " '+refs/heads/*:refs/heads/*' --prune");
		}
		$after = shell_exec("git show-ref");
		if($after == $before)
		{
			if(($argv[1] ?? '') != '-f')
			{
				continue;
			}
		}

		$db_commits_ref = $database->getReference("projects/" . $remote . "/commits");
		$db_commits = $db_commits_ref->getSnapshot()->getValue();

		exec('git log --all --pretty="%H %G? %GK"', $commit_rows);
		foreach($commit_rows as $row)
		{
			$columns = explode(' ', $row, 3);
			$hash = $columns[0];

			if(isset($db_commits[$hash]))
			{
				echo ".";
				continue;
			}

			echo $columns[1];

			switch($columns[1])
			{
				case 'N':
				{
					$db_commits_ref->update([ $hash => [
                  'status' => 'none',
					]]);
					break;
				}
				case 'G':
				{
					$db_commits_ref->update([ $hash => [
						'status' => 'good',
						'signature' => $columns[2],
					]]);
					break;
				}
				default:
				{
					print_r([$row, $path, $remot]);
				}
			}
		}
	}

	echo PHP_EOL;
