// ==UserScript==
// @name        bitbucket signed commits
// @namespace   puggan
// @include     https://bitbucket.org*/*/commits/all
// @include     https://bitbucket.org*/*/commits/branch/*
// @version     1
// @grant       none
// @noframes
// ==/UserScript==

console.log('gm init');
if(!window.gm_loaded)
{
	window.gm_loaded = true;
	var head = document.getElementsByTagName('head')[0];
	var fb_script = document.getElementById('firebase_script');
	if(!fb_script)
	{
		console.log('fb_script init');
		fb_script = document.createElement('script');
		fb_script.id = 'firebase_script';
		fb_script.src = 'https://www.gstatic.com/firebasejs/3.9.0/firebase.js';
		fb_script.onload = function()
		{
			if(!firebase)
			{
				console('init failed');
				return;
			}
			fb_script.onload = null;

			console.log('init gitsigned');

			// Initialize Firebase
			var config = {
				apiKey: "AIzaSyDvb-W4uX-Ba3KgqVV2icLWFXiKrZGsU8k",
				authDomain: "gitsigns.firebaseapp.com",
				databaseURL: "https://gitsigns.firebaseio.com",
				projectId: "gitsigns",
				storageBucket: "gitsigns.appspot.com",
				messagingSenderId: "803990850172"
			};
			firebase.initializeApp(config);

			var fb = {};
			fb.database = firebase.database();
			fb.keys = {};
			fb.database.ref('/keymeta')
				.on('value', function(s)
				{
					console.log('Keys loaded');
					fb.keys = s.val();
					fb.update();
				});
			fb.project = {};
			fb.database.ref('projects/' + location.pathname.split('/')
					.slice(1, 3)
					.join('/'))
				.on('value', function(s)
				{
					console.log('Projecte loaded');
					fb.project = s.val();
					fb.update();
				});
			fb.fetchlinks = function()
			{
				$('a.hash')
					.each(function(row_nr, a)
					{
						if(!a.sign)
						{
							var s = document.createElement('span');
							s.className = 'signstatus';
							s.innerHTML = '?';
							s.hash = a.pathname.split('/')[4];
							s.status = 'pending';
							s.style.backgroundColor = 'PowderBlue';
							s.style.paddingLeft = '2px';
							s.style.paddingRight = '2px';
							if(a.nextElementSibling)
							{
								a.parentNode.replaceChild(s, a.nextElementSibling);
							}
							else
							{
								a.parentNode.appendChild(s);
							}
							a.sign = s;
						}
					});
			};
			fb.update = function()
			{
				console.log('gitsigned update');
				fb.fetchlinks();
				$('span.signstatus')
					.each(function(row_nr, s)
					{
						if(s.hash && s.status === 'pending')
						{
							//console.log([s.hash, fb.project, fb.project && fb.project.commits, fb.project && fb.project.commits && fb.project.commits[s.hash], fb.project && fb.project.commits && fb.project.commits[s.hash] && fb.project.commits[s.hash].status])
							if(fb.project && fb.project.commits && fb.project.commits[s.hash] && fb.project.commits[s.hash].status)
							{
								var status = fb.project.commits[s.hash].status;
								s.status = status;
								s.className = 'signstatus ' + status;
								if(status === 'good')
								{
									s.innerHTML = '✓';
									s.style.backgroundColor = 'LightGreen';
									if(fb.project.commits[s.hash].signature && fb.keys[fb.project.commits[s.hash].signature])
									{
										s.title = fb.keys[fb.project.commits[s.hash].signature].email
									}
								}
								else if(status === 'bad')
								{
									s.innerHTML = 'E';
									s.style.backgroundColor = 'red';
								}
								else if(status === 'signed')
								{
									s.innerHTML = 'S';
									s.style.backgroundColor = 'yellow';
								}
								else if(status === 'none')
								{
									s.innerHTML = '-';
									s.style.backgroundColor = 'snow';
								}
							}
						}
					});
			};

			window.puggan = {};
			window.puggan.fb = fb;
			console.log(fb);
		};
		head.appendChild(fb_script);
	}
}
